# Ejercicio-bci

Un sistema de alta de usuarios, ejercicio solicitado por bci.

## Trabajar con en su IDE

### Prerrequisitos
Los siguientes elementos deben estar instalados en su sistema:
* Java 1.8 o superior
* [git command line tool](https://help.github.com/articles/set-up-git)
* Eclipse IDE, o uno de su preferencia
* [Postman](https://www.postman.com/downloads/)

### Pasos:

1) Ejecutar como línea de comandos:
    ```
    git clone https://gitlab.com/bigsoft_cl/ejerciciobci.git
    ```
2) Importar proyecto en Eclipse IDE:
    ```
    File -> Import -> Maven -> Existing Maven project
    ```

    lanzar app en Eclipse (haga clic derecho en el proyecto y `Run As -> Spring Boot App`).

3) Curls
   * Obtener Token de Autorización
     ```
       curl --location 'localhost:8090/token' \
       --header 'Content-Type: application/x-www-form-urlencoded' \
       --header 'Cookie: JSESSIONID=57CCEE7DBF2C307152614A0CDFA48170' \
       --data-urlencode 'user=miguel' \
       --data-urlencode 'password=miguel'
     ```

   * Registro de Usuario
     ```
			   curl --location 'localhost:8090/api/registro' \
		--header 'Content-Type: application/json' \
		--header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJzb2Z0dGVrSldUIiwic3ViIjoibWlndWVsIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIl0sImlhdCI6MTY4ODE4MzU1OSwiZXhwIjoxNjg4MTg0MTU5fQ.xZVX5Vg9gwmI6vYNVWWkKcgT3C6wWXhL7dOLoMlPM5NI1eEoXgfcD_ENrJFAkQbfZXytsrMPra72444g_-ZiIA' \
		--header 'Cookie: JSESSIONID=C9E6EE4BD3499C5491A4C64B4DD91D3E' \
		--data-raw '{
			"name": "Juan Rodriguez",
			"email": "juan@rodriguez.org",
			"password": "Hunter2!",
			"phones": [
				{
					"number": "123456789",
					"citycode": "1",
					"contrycode": "1"
				}
			]
		}'
     ```

    Swagger [http://localhost:8090/swagger-ui.html](http://localhost:8090/swagger-ui.html) en tu navegador.

![image description](https://inmovision.cl/bciProyecto/Swagger.png)


Base de datos H2 [http://localhost:8090/h2-ui/](http://localhost:8090/h2-ui/) en tu navegador.
* Driver Class: org.h2.Driver
* JDBC URL: jdbc:h2:mem:bcidb
* User Name: bcih2
* Password: bcih2

![image description](https://inmovision.cl/bciProyecto/h2Bd.png)

Nota: no se necesita script, ni instalación de la base de datos, ya que se encuentra embebida y el esquema se genera automaticamente al igual que la precarga de algunas ciudades y paises.

Paises por defecto

![image description](https://inmovision.cl/bciProyecto/paises.png)


Ciudades por defecto

![image description](https://inmovision.cl/bciProyecto/Ciudades.png)


## Diagrama.

![image description](https://inmovision.cl/bciProyecto/ModeloSolucion.png)

