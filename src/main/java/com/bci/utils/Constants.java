package com.bci.utils;

/**
 * The Class Constants.
 * @author Miguel Vergara
 */
public abstract class Constants {
	
	/**  MENSAJES DE ERROR - INI *. */
	public static final String USER_DISABLE = "Usuario deshabilitado";
	
	/** The Constant NO_CONTENT. */
	public static final String NO_CONTENT = "No se encontraron datos para el recurso consultado";
	
	/** The Constant RESOURCE_CONFLICT. */
	public static final String RESOURCE_CONFLICT = "El recurso ya existe";
	
	/** The Constant USER_NOT_FOUND. */
	public static final String USER_NOT_FOUND = "El usuario no fue encontrado";
	
	/** The Constant INVALID_PASSWORD. */
	public static final String INVALID_PASSWORD = "Contraseña no cumple con complejidad";
	
	/** The Constant INVALID_USER. */
	public static final String INVALID_USER = "Usuario inválido";
	
	/** The Constant VALIDATION_ERROR. */
	public static final String VALIDATION_ERROR = "Validación errónea";
	
	/** The Constant EMAIL_ALREADY_EXISTS. */
	public static final String EMAIL_ALREADY_EXISTS = "El correo ya registrado";
	
	/** The Constant INVALID_EMAIL. */
	public static final String INVALID_EMAIL = "Correo ingresado no cumple con formato";
	
	/** The Constant USER_NOT_CREATED. */
	public static final String USER_NOT_CREATED = "El usuario no fue creado";
	
	/** The Constant DEFAULT_ERROR. */
	public static final String DEFAULT_ERROR = "Error interno";
	
	/** The Constant INVALID_CITY. */
	public static final String INVALID_CITY = "citycode informado inválido";
	
	/** The Constant EMPTY_NUMBER. */
	public static final String EMPTY_NUMBER = "El campo number no fue informado";
	
	/** The Constant LENGTH_NUMBER. */
	public static final String LENGTH_NUMBER = "El campo number no es de largo 9";

	/** The Constant EMPTY_CITY. */
	public static final String EMPTY_CITY = "El campo citycode no fue informado";
	
	/** The Constant EMPTY_CONTRY. */
	public static final String EMPTY_CONTRY = "El campo contrycode no fue informado";
	
	/** The Constant INVALID_COUNTRY. */
	public static final String INVALID_COUNTRY = "contrycode informado inválido";
	
	/** The Constant INVALID_PHONES_ARRAY. */
	public static final String INVALID_PHONES_ARRAY = "No se informo arreglo phones.";
	
	/** The Constant UNAUTHORIZED. */
	public static final String UNAUTHORIZED = "Usuario no autorizado";
	
	/** The Constant IDENTIFICADOR_CITY. */
	public static final String IDENTIFICADOR_CITY = "ciudad";
	
	/** The Constant IDENTIFICADOR_COUNTRY. */
	public static final String IDENTIFICADOR_COUNTRY = "pais";
	
	/** The Constant ESTADO_USER_DEFAULT. */
	public static final String ESTADO_USER_DEFAULT = "ACTIVE";
	
	/** The Constant REGEXP_EMAIL. */
	public static final String REGEXP_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
}
