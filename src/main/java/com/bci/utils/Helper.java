package com.bci.utils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bci.entity.City;
import com.bci.entity.Country;
import com.bci.entity.Phones;
import com.bci.entity.User;
import com.bci.entity.UserStatus;
import com.bci.exception.CustomException;
import com.bci.exception.ErrorCodeEnumerator;
import com.bci.model.UserRequest;
import com.bci.model.UserResponse;
import com.bci.repository.EjercicioBciRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

/** The Constant log. */
@Log4j2
@Component

/**
 * Instantiates a new helper.
 * @author Miguel Vergara
 */
@RequiredArgsConstructor
public class Helper {
	
	/** The password pattern. */
	@Value("${expresiones.pass}")
	private String passwordPattern;
	
    /** The user repository. */
    @Autowired
    EjercicioBciRepository userRepository;
	
	/**
	 * Setea datos.
	 *
	 * @param request the request
	 * @param uuid the uuid
	 * @return the user
	 */
	public User seteaDatos(UserRequest request, UUID uuid) {
		User user = new User();
		try {
			user.setUuid(uuid.toString());
			user.setName(request.getName());
			user.setEmail(request.getEmail());
			user.setPassword(request.getPassword());
			UserStatus userStatus = new UserStatus();
			userStatus.setUserStatusId(1L);
			userStatus.setUserStatusName(Constants.ESTADO_USER_DEFAULT);
			user.setUserStatus(userStatus);
			user.setLast_login(LocalDateTime.now(TimeZone.getTimeZone("America/Santiago").toZoneId()));
		} catch (Exception e) {
			log.error("Error: [{}]", e);
		}
		return user;
	}
	
	/**
	 * Phones.
	 *
	 * @param request the request
	 * @param userSave the user save
	 * @return the list
	 */
	public List<Phones> phones(UserRequest request, User userSave) {
		Phones phone = null;
		List<Phones> listaPhones = new ArrayList<Phones>();
		try {
				for (com.bci.model.Phones phones : request.getPhones()) {
					phone = new Phones();
					Country country = new Country();
					country.setCountryId(phones.getContrycode());
					phone.setCountry(country);
					City city = new City();
					city.setCityId(phones.getCitycode());
					phone.setCity(city);
					phone.setPhoneNumber(phones.getNumber());
					phone.setUserId(userSave.getUserId());
					listaPhones.add(phone);
				}
		} catch (Exception e) {
			log.error("Error: [{}]", e);
		}
		return listaPhones;
	}
	
	/**
	 * Creates the response.
	 *
	 * @param userSave the user save
	 * @param uuid the uuid
	 * @return the user response
	 */
	public UserResponse createResponse(User userSave, UUID uuid) {
		UserResponse response = new UserResponse();
		try {
			response.setUuid(uuid.toString());
			response.setCreated(userSave.getCreated().toString());
			response.setModified(userSave.getModified().toString());
			response.setLast_login(userSave.getCreated().toString());
			response.setToken("");
			response.setIsactive(true);
		} catch (Exception e) {
			log.error("Error: [{}]", e);
		}
		return response;
	}
	
	/**
	 * Validate format email.
	 *
	 * @param emailAddress the email address
	 * @param regexPattern the regex pattern
	 * @return true, if successful
	 */
	public void validateFormatEmail(String emailAddress, String regexPattern) {
    	Pattern pattern = Pattern.compile(regexPattern);
        Matcher matcher = pattern.matcher(emailAddress);
        if(!matcher.matches()) {
        	log.info("FORMATO EMAIL INVALIDO.");
        	throw new CustomException(ErrorCodeEnumerator.INVALID_EMAIL);
        }
	}
	
    /**
     * Valid password.
     *
     * @param password the password
     * @return true, if successful
     */
    public boolean validPassword(String password) {
    	Pattern pattern = Pattern.compile(passwordPattern);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }
    
    
    /**
     * Valida array phone.
     *
     * @param request the request
     * @return the map
     */
    public  Map<String, Integer> validaArrayPhone(UserRequest request) {
        Map<String, Integer> mapCitiesCountries = new HashMap<>();	
    	if(null != request && null != request.getPhones() && !request.getPhones().isEmpty()) {
				for (com.bci.model.Phones aPhone : request.getPhones()) {
					validNumber(aPhone);
					validCity(aPhone);
					mapCitiesCountries.put(Constants.IDENTIFICADOR_CITY, aPhone.getCitycode());
					mapCitiesCountries.put(Constants.IDENTIFICADOR_COUNTRY, aPhone.getContrycode());
				}
				
			}else {
				log.info("NO SE INFORMA ARREGLO PHONES.");
	        	throw new CustomException(ErrorCodeEnumerator.INVALID_ARRAY_PHONES);
			}
    	
    	return  mapCitiesCountries;
	}
	
    
    /**
     * Valid number.
     *
     * @param aPhone the a phone
     */
    public void validNumber(com.bci.model.Phones aPhone) {
    	if(null == aPhone.getNumber()) {
			log.info("NO SE INFORMA CAMPO NUMBER.");
        	throw new CustomException(ErrorCodeEnumerator.EMPTY_NUMBER);
		}
		
		if(null != aPhone.getNumber() && String.valueOf(aPhone.getNumber()).length() != 9 ) {
			log.info("EL LARGO DEL CAMPO NUMBER NO ES VALIDO.");
        	throw new CustomException(ErrorCodeEnumerator.LENGTH_NUMBER);
		}
    }
    
    
    /**
     * Valid city.
     *
     * @param aPhone the a phone
     */
    public void validCity(com.bci.model.Phones aPhone) {
    	if(null == aPhone.getCitycode()) {
			log.info("NO SE INFORMA CAMPO CITYCODE.");
        	throw new CustomException(ErrorCodeEnumerator.EMPTY_CITY);
		}
    }
    
    /**
     * Valid country.
     *
     * @param aPhone the a phone
     */
    public void validCountry(com.bci.model.Phones aPhone) {
    	if(null == aPhone.getContrycode()) {
			log.info("NO SE INFORMA CAMPO CITYCODE.");
			throw new CustomException(ErrorCodeEnumerator.EMPTY_CONTRY);
		}
    }
    /**
     * Valida ciudad.
     *
     * @param mapCitiesCountries the map cities countries
     * @param listCities the list cities
     */
    public void validaCiudad(Map<String, Integer> mapCitiesCountries, List<City> listCities) {
    	boolean encontrada = false;
    	for (Map.Entry<String, Integer> entry : mapCitiesCountries.entrySet()) {
    	   if(Constants.IDENTIFICADOR_CITY.equalsIgnoreCase(entry.getKey())) {
    		   for (City city : listCities) {
				if(entry.getValue().equals(city.getCityId())) {
					encontrada = true;
					break;
				}
    		   }
    		   if(!encontrada) {
					throw new CustomException(ErrorCodeEnumerator.INVALID_CITY);
				}
    	   }
    	}
    }
    
    /**
     * Valida pais.
     *
     * @param mapCitiesCountries the map cities countries
     * @param listCountries the list countries
     */
    public void validaPais(Map<String, Integer> mapCitiesCountries, List<Country> listCountries) {
    	boolean encontrada = false;
    	for (Map.Entry<String, Integer> entry : mapCitiesCountries.entrySet()) {
    	   if(Constants.IDENTIFICADOR_COUNTRY.equalsIgnoreCase(entry.getKey())) {
    		   for (Country country : listCountries) {
				if(entry.getValue().equals(country.getCountryId())) {
					encontrada = true;
					break;
				}
    		   }
    		   if(!encontrada) {
    			   throw new CustomException(ErrorCodeEnumerator.INVALID_COUNTRY);
				}
    	   }
    	}
    }
}
