package com.bci.service;

import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bci.exception.CustomException;
import com.bci.exception.ErrorCodeEnumerator;
import com.bci.model.UserRequest;
import com.bci.repository.CityRepository;
import com.bci.repository.CountryRepository;
import com.bci.repository.EjercicioBciRepository;
import com.bci.utils.Constants;
import com.bci.utils.Helper;

import lombok.extern.log4j.Log4j2;


/**
 * The Class EjercicioBciServiceImpl.
 * @author Miguel Vergara
 */

/** The Constant log. */
@Log4j2
@Component
public class EjercicioBciServiceImpl {

    /** The user repository. */
    @Autowired
    EjercicioBciRepository userRepository;
    
    /** The cityrepository. */
    @Autowired
    CityRepository cityrepository;
    
    /** The country repository. */
    @Autowired
    CountryRepository countryRepository;
    
    /** The user helper. */
    @Autowired
    Helper helper;
	
    
    /**
     * Validaciones.
     *
     * @param request the request
     * @param uuid the uuid
     */
    public void validaciones(UserRequest request, UUID uuid) {
    	helper.validateFormatEmail(request.getEmail().toLowerCase(), Constants.REGEXP_EMAIL);
		existeEmailBd(request);
		validPassword(request, uuid);
		Map<String, Integer> mapCitiesCountries = helper.validaArrayPhone(request);
		helper.validaCiudad(mapCitiesCountries, cityrepository.findAll());
		helper.validaPais(mapCitiesCountries, countryRepository.findAll());
    }
    
	
	/**
	 * Existe email bd.
	 *
	 * @param request the request
	 */
	public void existeEmailBd(UserRequest request) {
		if (userRepository.existsByEmail(request.getEmail().toLowerCase())) {
			log.info("EMAIL YA EXISTE.");
			throw new CustomException(ErrorCodeEnumerator.EMAIL_ALREADY_EXISTS);
		}
	}
	
	/**
	 * Valid password.
	 *
	 * @param request the request
	 * @param uuid the uuid
	 * @return the user
	 */
	public void validPassword(UserRequest request, UUID uuid) {
		if(!helper.validPassword(request.getPassword())) {
			log.info("FORMATO CONTRASEÑA INVALIDO.");
			throw new CustomException(ErrorCodeEnumerator.INVALID_PASSWORD);
		}
	}
}
