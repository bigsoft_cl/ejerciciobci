package com.bci.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.bci.entity.User;
import com.bci.exception.CustomException;
import com.bci.exception.ErrorCodeEnumerator;
import com.bci.model.UserRequest;
import com.bci.model.UserResponse;
import com.bci.repository.EjercicioBciRepository;
import com.bci.repository.PhonesRepository;
import com.bci.utils.Helper;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

/** The Constant log. */
@Log4j2
@Service

/**
 * Instantiates a new ejercicio bci service.
 * @author Miguel Vergara
 */
@RequiredArgsConstructor
public class EjercicioBciService {

    /** The user repository. */
    @Autowired
    EjercicioBciRepository userRepository;
    
    /** The phones repository. */
    @Autowired
    PhonesRepository phonesRepository;
    
    /** The user helper. */
    @Autowired
    Helper helper;
    
    /** The service impl. */
    @Autowired
    EjercicioBciServiceImpl serviceImpl;
    
    
	/**
	 * Creates the user.
	 *
	 * @param request the request
	 * @return the response entity
	 */
	public ResponseEntity<UserResponse> createUser(UserRequest request) {
		UserResponse response = null;
		try {
			UUID uuid = UUID.randomUUID();
			serviceImpl.validaciones(request, uuid);
			User userSave = userRepository.save(helper.seteaDatos(request, uuid));
			phonesRepository.saveAll(helper.phones(request, userSave));
			response = helper.createResponse(userSave, uuid);
			log.info("USER_ID: [{}]", userSave.getUserId());
		} catch (CustomException e) {
			throw e;
		} catch (Exception e) {
        	log.error("ERROR CREATE USER WEB: [{}]", e);
        	throw new CustomException("Error al crear usuario. ", ErrorCodeEnumerator.USER_NOT_CREATED);
		}
		return new ResponseEntity<UserResponse>(response, HttpStatus.CREATED);
	}

}
