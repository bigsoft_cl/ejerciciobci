package com.bci.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bci.exception.CustomException;
import com.bci.model.ErrorResponse;
import com.bci.model.PostOperator;
import com.bci.model.UserRequest;
import com.bci.service.EjercicioBciService;

/**
 * The Class EjercicioBciController.
 * @author Miguel Vergara
 */
@RestController
@RequestMapping("/api")
public class EjercicioBciController {
	
	/** The service. */
	@Autowired
	EjercicioBciService service;
	
    /**
     * Adds the user.
     *
     * @param request the request
     * @return the response entity
     */
    @PostMapping("/registro")
    public ResponseEntity<?> addUser(@RequestBody @Validated(PostOperator.class) UserRequest request) {
        try {
            return new ResponseEntity<>(service.createUser(request), HttpStatus.CREATED);
        } catch (CustomException e) {
        	return new ResponseEntity<>(new ErrorResponse(e.getErrorType(),e.getMessage()), e.getErrorType().getCode());
        }
    }

}
