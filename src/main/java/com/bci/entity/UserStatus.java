package com.bci.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Data

/**
 * Instantiates a new user status.
 */
@NoArgsConstructor

/**
 * Instantiates a new user status.
 * @author Miguel Vergara
 * @param userStatusId the user status id
 * @param userStatusName the user status name
 */
@AllArgsConstructor
@Entity
@Table(name = "user_status")
public class UserStatus {
    
    /** The user status id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_status_id")
    private Long userStatusId;

    /** The user status name. */
    @Column(name = "user_status_name", length = 128)
    private String userStatusName;
}
