package com.bci.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class Phones.
 */
@Entity
@Table(name="Phones")

/**
 * Instantiates a new phones.
 */
@NoArgsConstructor

/**
 * Instantiates a new phones.
 * @author Miguel Vergara
 * @param phoneId the phone id
 * @param userId the user id
 * @param phoneNumber the phone number
 * @param country the country
 * @param city the city
 */
@AllArgsConstructor

/**
 * Sets the city.
 *
 * @param city the new city
 */
@Setter

/**
 * Gets the city.
 *
 * @return the city
 */
@Getter

/**
 * To string.
 *
 * @return the java.lang. string
 */
@ToString
public class Phones {
	
	/** The phone id. */
	@Id
	@Column(name = "phone_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long phoneId;
	
	/** The user id. */
	@Column(name = "user_id")
	private Long userId;
    
    /** The phone number. */
    @Column(name = "phone_number")
    private Integer phoneNumber;
    
    /** The country. */
    @ManyToOne()
    @JoinColumn(name = "country_id")
    private Country country;
    
    /** The city. */
    @ManyToOne()
    @JoinColumn(name = "city_id")
    private City city;
}
