package com.bci.entity;

import java.time.LocalDateTime;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class User.
 */
@Entity
@Table(name="Users")

/**
 * Instantiates a new user.
 */
@NoArgsConstructor

/**
 * Instantiates a new user.
 * @author Miguel Vergara
 * @param userId the user id
 * @param uuid the uuid
 * @param created the created
 * @param modified the modified
 * @param name the name
 * @param email the email
 * @param password the password
 * @param userStatus the user status
 * @param last_login the last login
 */
@AllArgsConstructor

/**
 * Sets the last login.
 *
 * @param last_login the new last login
 */
@Setter

/**
 * Gets the last login.
 *
 * @return the last login
 */
@Getter

/**
 * To string.
 *
 * @return the java.lang. string
 */
@ToString
public class User {
	
    /** The user id. */
    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long userId;
    
    /** The uuid. */
    @Column(name = "uuid", length = 128)
    private String uuid;
    
    /** The created. */
    @CreatedDate
    @Column(name = "created", nullable = false, updatable = false)
    private LocalDateTime created;
    
    /** The modified. */
    @LastModifiedDate
    @Column(name = "modified", nullable = false)
    private LocalDateTime modified;

    /** The name. */
    @Column(name = "name", length = 128)
    private String name;
    
    /** The email. */
    @Column(name = "email", length = 128)
    private String email;
    
    /** The password. */
    @Column(name = "password", length = 128)
    private String password;
    
    /** The user status. */
    @ManyToOne()
    @JoinColumn(name = "user_status_id")
    private UserStatus userStatus;
    
    /** The last login. */
    @Column(name = "last_login")
    private LocalDateTime last_login;
    
    /**
     * Pre persist.
     */
    @PrePersist
    public void prePersist() {
    	created = LocalDateTime.now(TimeZone.getTimeZone("America/Santiago").toZoneId());
    	modified = LocalDateTime.now(TimeZone.getTimeZone("America/Santiago").toZoneId());
    }

    /**
     * Pre update.
     */
    @PreUpdate
    public void preUpdate() {
    	modified = LocalDateTime.now(TimeZone.getTimeZone("America/Santiago").toZoneId());
    }

}
