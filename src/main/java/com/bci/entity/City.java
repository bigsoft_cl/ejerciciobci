package com.bci.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Data

/**
 * Instantiates a new city.
 */
@NoArgsConstructor

/**
 * Instantiates a new city.
 * @author Miguel Vergara
 * @param cityId the city id
 * @param cityName the city name
 */
@AllArgsConstructor
@Entity
@Table(name = "cities")
public class City {
    
    /** The city id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "city_id")
	private Integer cityId;

    /** The city name. */
    @Column(name = "city_name", length = 64)
    private String cityName;
}
