package com.bci.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Data

/**
 * Instantiates a new country.
 */
@NoArgsConstructor

/**
 * Instantiates a new country.
 * @author Miguel Vergara
 * @param countryId the country id
 * @param countryName the country name
 */
@AllArgsConstructor
@Entity
@Table(name = "countries")
public class Country {
    
    /** The country id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "country_id")
	private Integer countryId;

	/** The country name. */
	@Column(name = "country_name", length = 64)
	private String countryName;
}
