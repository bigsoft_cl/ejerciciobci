package com.bci.vo;

import lombok.Data;

/**
 * Instantiates a new user token.
 * @author Miguel Vergara
 */
@Data
public class UserToken {

	/** The token. */
	private String token;

}
