package com.bci.exception;

import org.springframework.http.HttpStatus;

import com.bci.utils.Constants;

/**
 * The Enum ErrorCodeEnumerator.
 * @author Miguel Vergara
 */
public enum ErrorCodeEnumerator {
  
  /** The invalid city. */
  INVALID_CITY(HttpStatus.BAD_REQUEST, Constants.INVALID_CITY),
  
  /** The invalid country. */
  INVALID_COUNTRY(HttpStatus.BAD_REQUEST, Constants.INVALID_COUNTRY),
  
  /** The no content. */
  NO_CONTENT(HttpStatus.NO_CONTENT, Constants.NO_CONTENT),
  
  /** The resource conflict. */
  RESOURCE_CONFLICT(HttpStatus.CONFLICT, Constants.RESOURCE_CONFLICT),
  
  /** The validation error. */
  VALIDATION_ERROR(HttpStatus.BAD_REQUEST, Constants.VALIDATION_ERROR),
  
  /** The email already exists. */
  EMAIL_ALREADY_EXISTS(HttpStatus.CONFLICT, Constants.EMAIL_ALREADY_EXISTS),
  
  /** The invalid email. */
  INVALID_EMAIL(HttpStatus.BAD_REQUEST, Constants.INVALID_EMAIL),
  
  /** The invalid array phones. */
  INVALID_ARRAY_PHONES(HttpStatus.PARTIAL_CONTENT, Constants.INVALID_PHONES_ARRAY),
  
  /** The empty number. */
  EMPTY_NUMBER(HttpStatus.PARTIAL_CONTENT, Constants.EMPTY_NUMBER),
  
  /** The length number. */
  LENGTH_NUMBER(HttpStatus.PARTIAL_CONTENT, Constants.LENGTH_NUMBER),
  
  /** The empty city. */
  EMPTY_CITY(HttpStatus.PARTIAL_CONTENT, Constants.EMPTY_CITY),
  
  /** The empty contry. */
  EMPTY_CONTRY(HttpStatus.PARTIAL_CONTENT, Constants.EMPTY_CONTRY),
  
  /** The user not created. */
  USER_NOT_CREATED(HttpStatus.CONFLICT, Constants.USER_NOT_CREATED),
  
  /** The default error. */
  DEFAULT_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, Constants.DEFAULT_ERROR),
  
  /** The unauthorized. */
  UNAUTHORIZED(HttpStatus.UNAUTHORIZED, Constants.UNAUTHORIZED),
  
  /** The user not found. */
  USER_NOT_FOUND(HttpStatus.BAD_REQUEST, Constants.USER_NOT_FOUND),
  
  /** The invalid user. */
  INVALID_USER(HttpStatus.BAD_REQUEST, Constants.USER_NOT_FOUND),
  
  /** The invalid password. */
  INVALID_PASSWORD(HttpStatus.CONFLICT, Constants.INVALID_PASSWORD),
  
  /** The user disable. */
  USER_DISABLE(HttpStatus.BAD_REQUEST, Constants.USER_DISABLE),
  ;

  /** The code. */
  private final HttpStatus code;
  
  /** The description. */
  private final String description;

  /**
   * Instantiates a new error code enumerator.
   *
   * @param code the code
   * @param description the description
   */
  ErrorCodeEnumerator(HttpStatus code, String description) {
    this.code = code;
    this.description = description;
  }

  /**
   * To string.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return "ErrorCodeEnumerator{" + "code=" + code + ", description='" + description + '\'' + '}';
  }

  /**
   * Gets the code.
   *
   * @return the code
   */
  public HttpStatus getCode() {
    return code;
  }

  /**
   * Gets the description.
   *
   * @return the description
   */
  public String getDescription() {
    return description;
  }
}
