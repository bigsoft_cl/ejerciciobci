package com.bci.exception;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.bci.model.ErrorResponse;

/**
 * The Class ExceptionController.
 * @author Miguel Vergara
 */
@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {

    /**
     * Handle SQL exception.
     *
     * @param request the request
     * @param ex the ex
     * @return the error response
     */
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(SQLException.class)
    public @ResponseBody ErrorResponse handleSQLException(HttpServletRequest request, Exception ex){
    	ErrorResponse response = new ErrorResponse (HttpStatus.UNAUTHORIZED, "Unauthorized, Token expirado"); 
        return response;
    }
}
