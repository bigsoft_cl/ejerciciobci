package com.bci.exception;

/**
 * The Class CustomException.
 * @author Miguel Vergara
 */
public class CustomException extends RuntimeException {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;
	
  /** The error type. */
  private ErrorCodeEnumerator errorType;

  /**
   * Instantiates a new custom exception.o
   *
   * @param errorType the error type
   */
  public CustomException(ErrorCodeEnumerator errorType) {
    super(errorType.getDescription());
    this.setErrorType(errorType);
  }

  /**
   * Instantiates a new custom exception.
   *
   * @param message the message
   */
  public CustomException(String message) {
    super(message);
    this.setErrorType(ErrorCodeEnumerator.DEFAULT_ERROR);
  }

  /**
   * Instantiates a new custom exception.
   *
   * @param message the message
   * @param errorType the error type
   */
  public CustomException(String message, ErrorCodeEnumerator errorType) {
    super(message);
    this.setErrorType(errorType);
  }

   /**
    * Gets the error type.
    *
    * @return the errorType
    */
  public ErrorCodeEnumerator getErrorType() {
	  return errorType;
  }
	
   /**
    * Sets the error type.
    *
    * @param errorType the errorType to set
    */
  public void setErrorType(ErrorCodeEnumerator errorType) {
	  this.errorType = errorType;
  }

}
