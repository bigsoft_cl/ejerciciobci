package com.bci.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Data

/**
 * Instantiates a new phones.
 */
@NoArgsConstructor

/**
 * Instantiates a new phones.
 * @author Miguel Vergara
 * @param number the number
 * @param citycode the citycode
 * @param contrycode the contrycode
 */
@AllArgsConstructor
public class Phones {
	
	/** The number. */
	private Integer number;
	
	/** The citycode. */
	private Integer citycode;
	
	/** The contrycode. */
	private Integer contrycode;
}
