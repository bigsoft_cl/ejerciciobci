package com.bci.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Data

/**
 * Instantiates a new user response.
 */
@NoArgsConstructor

/**
 * Instantiates a new user response.
 * @author Miguel Vergara
 * @param uuid the uuid
 * @param created the created
 * @param modified the modified
 * @param last_login the last login
 * @param token the token
 * @param isactive the isactive
 */
@AllArgsConstructor
public class UserResponse {
	
	/** The uuid. */
	private String uuid;
	
	/** The created. */
	private String created;
	
	/** The modified. */
	private String modified;
	
	/** The last login. */
	private String last_login;
	
	/** The token. */
	private String token;
	
	/** The isactive. */
	private Boolean isactive;
}
