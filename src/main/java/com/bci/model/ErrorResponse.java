package com.bci.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import com.bci.exception.ErrorCodeEnumerator;


/**
 * Gets the mensaje.
 *
 * @return the mensaje
 */
@Getter

/**
 * Sets the mensaje.
 *
 * @param mensaje the new mensaje
 */
@Setter

/**
 * Instantiates a new error response.
 * @author Miguel Vergara
 * @param code the code
 * @param mensaje the mensaje
 */
@AllArgsConstructor

/**
 * Instantiates a new error response.
 */
@NoArgsConstructor
public class ErrorResponse {

  /** The code. */
  private int code;
  
  /** The mensaje. */
  private String mensaje;
  

  /**
   * Instantiates a new error response.
   *
   * @param errorCodeEnumerator the error code enumerator
   * @param message the message
   */
  public ErrorResponse(ErrorCodeEnumerator errorCodeEnumerator, String message) {
	  this.code = errorCodeEnumerator.getCode().value();
	  this.mensaje = message;
  }

  /**
   * Instantiates a new error response.
   *
   * @param httpStatus the http status
   * @param message the message
   */
  public ErrorResponse(HttpStatus httpStatus, String message) {
	  this.code = httpStatus.value();
	  this.mensaje = message;
  }
  
}
