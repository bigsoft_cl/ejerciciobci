package com.bci.model;


import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * To string.
 *
 * @return the java.lang. string
 */
@Data

/**
 * Instantiates a new user request.
 */
@NoArgsConstructor

/**
 * Instantiates a new user request.
 * @author Miguel Vergara
 * @param name the name
 * @param email the email
 * @param password the password
 * @param phones the phones
 */
@AllArgsConstructor
public class UserRequest {
	
	/** The name. */
	@NotNull(groups = {PostOperator.class, PostOperatorWeb.class})
	@NotBlank(groups = {PostOperator.class, PostOperatorWeb.class})
    private String name;
	
	/** The email. */
	@NotNull(groups = {PostOperator.class, PostOperatorWeb.class})
	@NotBlank(groups = {PostOperator.class, PostOperatorWeb.class})
    private String email;
	
	/** The password. */
	@NotNull(groups = {PostOperator.class, PostOperatorWeb.class})
	@NotBlank(groups = {PostOperator.class, PostOperatorWeb.class})
    private String password;
	
    /** The phones. */
    private List<Phones> phones;
}
