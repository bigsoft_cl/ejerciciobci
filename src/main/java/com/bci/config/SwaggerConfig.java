package com.bci.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class SwaggerConfig.
 * @author Miguel Vergara
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig extends WebMvcConfigurationSupport {

  /** The app name. */
  @Value("${swagger.application.name}")
  private String appName;

  /** The app build version. */
  @Value("${swagger.application.build.version}")
  private String appBuildVersion;

  /** The app description. */
  @Value("${swagger.application.description}")
  private String appDescription;

  /** The contact name. */
  @Value("${swagger.contact.name}")
  private String contactName;

  /** The contact email. */
  @Value("${swagger.contact.email}")
  private String contactEmail;

  /** The contact url. */
  @Value("${swagger.contact.url}")
  private String contactUrl;

  /**
   * Api docket.
   *
   * @return the docket
   */
  @Bean
  public Docket apiDocket() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.basePackage("com.bci.controller"))
        .paths(PathSelectors.any())
        .build()
        .apiInfo(getApiInfo())
        .useDefaultResponseMessages(false);
  }

  /**
   * Gets the api info.
   *
   * @return the api info
   */
  private ApiInfo getApiInfo() {

    return new ApiInfo(
        appName,
        appDescription,
        appBuildVersion,
        null,
        new Contact(contactName, contactUrl, contactEmail),
        null,
        null,
        new ArrayList<>());
  }

  /**
   * Adds the resource handlers.
   *
   * @param registry the registry
   */
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("swagger-ui.html")
            .addResourceLocations("classpath:/META-INF/resources/");

    registry.addResourceHandler("/webjars/**")
            .addResourceLocations("classpath:/META-INF/resources/webjars/");
  }

  /**
   * Adds the argument resolvers.
   *
   * @param argumentResolvers the argument resolvers
   */
  @Override
  public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
    argumentResolvers.add( new PageableHandlerMethodArgumentResolver());
  }
}

