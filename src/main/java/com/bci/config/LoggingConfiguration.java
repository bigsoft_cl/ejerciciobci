package com.bci.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The Class LoggingConfiguration.
 * @author Miguel Vergara
 */
@Configuration
public class LoggingConfiguration {
   
   /**
    * Request response logging filter.
    *
    * @return the request and response logging filter
    */
   @Bean
   public RequestAndResponseLoggingFilter requestResponseLoggingFilter() {
       return new RequestAndResponseLoggingFilter();
   }
}  
