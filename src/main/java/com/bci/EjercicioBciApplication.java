package com.bci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * The Class EjercicioBciApplication.
 * @author Miguel Vergara 
 */
@EnableWebMvc
@SpringBootApplication
public class EjercicioBciApplication {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(EjercicioBciApplication.class, args);
	}

	
}
