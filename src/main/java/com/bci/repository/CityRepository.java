package com.bci.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bci.entity.City;

/**
 * The Interface CityRepository.
 * @author Miguel Vergara
 */
@Repository
public interface CityRepository extends JpaRepository<City, Long>{

}
