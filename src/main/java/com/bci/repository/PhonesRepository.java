package com.bci.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bci.entity.Phones;

/**
 * The Interface PhonesRepository.
 * @author Miguel Vergara
 */
@Repository
public interface PhonesRepository extends JpaRepository<Phones, Long> {

}
