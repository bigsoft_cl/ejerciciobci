package com.bci.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bci.entity.User;


/**
 * The Interface EjercicioBciRepository.
 * @author Miguel Vergara
 */
@Repository
public interface EjercicioBciRepository extends JpaRepository<User, Long> {
	
	/**
	 * Exists by email.
	 *
	 * @param email the email
	 * @return true, if successful
	 */
	boolean existsByEmail(String email);
}

