package com.bci.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bci.entity.Country;

/**
 * The Interface UserStatusRepository.
 * @author Miguel Vergara
 */
@Repository
public interface UserStatusRepository extends JpaRepository<Country, Long> {

}
