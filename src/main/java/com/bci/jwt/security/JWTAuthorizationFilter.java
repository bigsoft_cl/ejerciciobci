package com.bci.jwt.security;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import com.bci.exception.ExceptionController;
import com.bci.model.ErrorResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

/**
 * The Class JWTAuthorizationFilter.
 * @author Miguel Vergara
 */
public class JWTAuthorizationFilter extends OncePerRequestFilter {

	/** The header. */
	private final String HEADER = "Authorization";
	
	/** The prefix. */
	private final String PREFIX = "Bearer ";
	
	/** The secret. */
	private final String SECRET = "mySecretKey";

	/**
	 * Do filter internal.
	 *
	 * @param request the request
	 * @param response the response
	 * @param chain the chain
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
		try {
			if (checkJWTToken(request, response)) {
				Claims claims = validateToken(request);
				if (claims.get("authorities") != null) {
					setUpSpringAuthentication(claims);
				} else {
					SecurityContextHolder.clearContext();
				}
			} else {
				if(null != request.getParameterValues("user")) {
					SecurityContextHolder.clearContext();
				}else {
					throw new Exception();
				}
			}
			chain.doFilter(request, response);
		} catch(Exception ex) {
			  ExceptionController myExceptionController = new ExceptionController(); 
			  ErrorResponse errorDTO = myExceptionController.handleSQLException(request, ex); 
			  response.setStatus(errorDTO.getCode());
			  response.setContentType("application/json");
	          ObjectMapper mapper = new ObjectMapper();
	          PrintWriter out = response.getWriter(); 
	          out.print(mapper.writeValueAsString(errorDTO ));
	          out.flush();
	          return; 
		}
	}	

	/**
	 * Validate token.
	 *
	 * @param request the request
	 * @return the claims
	 */
	private Claims validateToken(HttpServletRequest request) {
		String jwtToken = request.getHeader(HEADER).replace(PREFIX, "");
		return Jwts.parser().setSigningKey(SECRET.getBytes()).parseClaimsJws(jwtToken).getBody();
	}

	/**
	 * Authentication method in Spring flow.
	 *
	 * @param claims the new up spring authentication
	 */
	private void setUpSpringAuthentication(Claims claims) {
		@SuppressWarnings("unchecked")
		List<String> authorities = (List<String>) claims.get("authorities");

		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(claims.getSubject(), null,
				authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
		SecurityContextHolder.getContext().setAuthentication(auth);

	}

	/**
	 * Check JWT token.
	 *
	 * @param request the request
	 * @param res the res
	 * @return true, if successful
	 */
	private boolean checkJWTToken(HttpServletRequest request, HttpServletResponse res) {
		String authenticationHeader = request.getHeader(HEADER);
		if (authenticationHeader == null || !authenticationHeader.startsWith(PREFIX))
			return false;
		return true;
	}

}
