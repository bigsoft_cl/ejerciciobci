package com.bci.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.bci.exception.CustomException;
import com.bci.exception.ErrorCodeEnumerator;
import com.bci.model.Phones;
import com.bci.model.UserRequest;
import com.bci.service.EjercicioBciService;

@SpringBootTest
public class EjercicioBciControllerTest {
	
	@InjectMocks
	private EjercicioBciController controller;
	@Mock
	private EjercicioBciService service;
	
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    void addUser_OK() {
    	UserRequest request = new UserRequest();
    	request.setEmail("asd@asd.cl");
    	request.setName("asd");
    	request.setPassword("aaa");
    	request.setPhones(null);
    	Phones phones = new Phones();
    	phones.setCitycode(1);
    	phones.setContrycode(2);
    	phones.setNumber(123);
    	List<Phones> listPhones = new ArrayList<>();
    	listPhones.add(phones);
    	request.setPhones(listPhones);
    	ResponseEntity entity = new ResponseEntity<>(HttpStatus.OK);
        Mockito.when(service.createUser(any(UserRequest.class))).thenReturn(entity);
        assertNotNull(controller.addUser(request));
    }
    
    @Test
    void addUser_NOK() throws IOException {
        CustomException exception = Mockito.mock(CustomException.class);
        doReturn(ErrorCodeEnumerator.USER_NOT_CREATED).when(exception).getErrorType();
        doThrow(exception).when(service).createUser(
                any(UserRequest.class));
        assertNotNull(controller.addUser(null));
    }

}
